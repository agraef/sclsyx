
sclsyx
======

This script converts musical tunings in Manuel Op de Coul's [Scala file
format](http://www.huygens-fokker.org/scala/scl_format.html) into system
exclusive messages, using either the 1-byte or the 2-byte encoding of
octave-based 12 tone scales, as described in the MTS a.k.a. [MIDI Tuning
Standard](http://www.midi.org/techspecs/midituning.php). The output can be
written either to stdout in human-readable form, as a binary sysex (.syx) file
which is ready to be used with your sysex librarian, or as a type 1 standard
MIDI file (SMF) which can be loaded by your MIDI sequencer or DAW program.
You can also just output the tuning as a list of cent values in floating point
format.

Please note that your MIDI instrument must support the MTS in order to use the
sysex tuning messages; most hardware synthesizers don't, but some software
instruments such as [FluidSynth](http://www.fluidsynth.org/) do. If your
instrument doesn't support MTS then it will most likely just ignore these
messages.

A few examples are included in this directory, please see the scl subdirectory
for many more. The Scala tuning (.scl) files included here can be found on the
[Scala website](http://www.huygens-fokker.org/scala/). We have only included
the octave (12 tone) based scales since these are the only scales that the
sclsyx program works with. The latest version of the complete archive can be
downloaded freely at <http://www.huygens-fokker.org/docs/scales.zip>.

The sclsyx script is free to use for whatever purpose, please check the
sclsyx.pure file for copyright and copying conditions. The script is written
in the author's Pure programming language, so you need to have the Pure
interpreter (0.58 or later) installed to run it; the MIDI file output also
requires the pure-midi module. You can find these at the Pure website,
<https://agraef.github.io/pure-lang/>. The script can be run "as is", or you
can compile it to a native executable by running `make`. Some other convenient
`make` targets are (these require that you run `make` beforehand):

* `make syx` translates the .scl files in the source directory to sysex (.syx)
  files. Likewise, `make midi` creates the corresponding MIDI (.mid) files.

* `make allsyx` builds the sysex files for all tunings in the scl
  subdirectory. `make allmidi` creates the corresponding MIDI files.

* `make clean` removes all created files, including the sclsyx executable.

You can also install the sclsyx executable on your PATH so that it can easily
be invoked from anywhere. The program is invoked as follows:

  sclsyx [options] scala-file(.scl) [sysex-file(.syx)]

The options are:

* `-h`, `--help`:     print a short help text
* `-r`, `--realtime`: realtime sysex (default is non-realtime)
* `-2`, `--2byte`:    2-byte encoding (default is 1-byte)
* `-d`, `--decimal`:  decimal output (implies `-p`)
* `-f`, `--float`:    floating point output (implies `-p`)
* `-m`, `--midi`:     MIDI file output (default is sysex file)
* `-p`, `--print`:    human-readable printout on stdout
* `-b`, `--basetone`: reference tone of the scale (0..11, default is 0)

The default output filename is the name of the scale file with new extension
`.syx` indicating a binary sysex file, or `.mid` indicating a standard MIDI
file if the `-m` option is used. If the `-p`, `-d`, or `-f` option is
specified, output goes to stdout in human-readable text format instead.

Otherwise the output is always an MTS scale/octave tuning in 1 or 2 byte
format, please check the [MTS](http://www.midi.org/techspecs/midituning.php)
for details. The tuning is shifted so that the given base tone is fixed at an
offset of 0 cent. The 1-byte tunings are whole cent offsets in the range
-64..+63. The 2-byte encoding offers an extended range of -100..+100 cents
with 14 bit resolution.

Enjoy! :)

2020-10-10 Albert Graef <aggraef@gmail.com>

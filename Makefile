
# How to invoke the sclsyx program.
SCLSYX = ./sclsyx
# This produces a realtime sysex and puts the tone a at 0ct. Adjust as needed.
FLAGS = -r -b9

scales = $(wildcard *.scl)
allscales = $(wildcard scl/*.scl)

syx = $(scales:.scl=.syx)
allsyx = $(allscales:.scl=.syx)

midi = $(scales:.scl=.mid)
allmidi = $(allscales:.scl=.mid)

all: sclsyx
syx: $(syx)
midi: $(midi)

allsyx:
	for x in scl/*.scl; do $(SCLSYX) $(FLAGS) "$$x"; done

allmidi:
	for x in scl/*.scl; do $(SCLSYX) $(FLAGS) -m "$$x"; done

clean:
	rm -f sclsyx *.syx *.mid scl/*.syx scl/*.mid

sclsyx: sclsyx.pure
	pure -c $< -o $@

%.syx: %.scl
	$(SCLSYX) $(FLAGS) "$<" "$@"

%.mid: %.scl
	$(SCLSYX) $(FLAGS) -m "$<" "$@"
